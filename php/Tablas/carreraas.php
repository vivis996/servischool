<?php
 
include "conexion.php";
$user_id=null;
$sql1= "select * from carreras";
$query = $con->query($sql1);

?>
<div class="panel panel-default">
    <br>
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <a data-toggle="modal" data-target="#myModal2" class="btn btn-default">Agregar <i class="glyphicon glyphicon-plus-sign"></i></a>
        </div>
    </div>
    <br><br>
    <!-- Modal -->
    <div class="modal" id="myModal2" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title">Agregar Carrera</h4>
                </div>
                <div class="modal-body">
                    <form role="form" method="post" action="./php/agregarlos.php">
                        <div class="form-group">
                            <label for="matricula">Id_carrera</label>
                            <input type="text" class="form-control" name="matricula" required>
                        </div>
                        <div class="form-group">
                            <label for="nombre">Carrera</label>
                            <input type="text" class="form-control" name="nombre" required>
                        </div>
                        <button type="submit" class="btn btn-default" name="btn-bsave">Agregar</button>
                    </form>
                </div>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="panel panel-default">
                <div class="panel-heading">
                    Todas las tablas
                </div>
                <!-- /.panel-headin -->
                <div class="panel-body">
                    <table class="table table-striped table-bordered table-hover" id="dataTables-example2">
                        <thead>
                            <tr>
                                <th>Id_carrera</th>
                                <th>Carrera</th>
                                <th>Acciones</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php while ($r=$query->fetch_array()):?>
                            <tr>
                                <td><?php echo $r["id_carrera"]; ?> </td>
                                <td><?php echo utf8_encode ($r["carrera"]);?></td>
                                <td style="width:150px;">
                                    <a href="./editar.php?id_carrera=<?php echo $r["id_carrera"];?>" class="btn btn-sm btn-warning">Editar</a>
                                    <a href="#" id="del-<?php echo $r["id_carrera"];?>" class="btn btn-sm btn-danger">Eliminar</a>
                                    <script>
                                        $('"<?php echo "#del".$r["id_carrera"];?>"').click(function(e){
                                            e.preventDefault();
                                            p = confirm("Estas seguro?");
                                            if(p){
                                                window.location="./php/eliminar.php?id_carrera="+<?php echo $r["id_carrera"];?>;
                                            }
                                        });
                                    </script>
                                </td>
                            </tr>
                            <?php endwhile;?>
                        </tbody>
                    </table>
                    <!-- /.table-responsive -->
                </div>
                <!-- /.panel-body -->
            </div>
            <!-- /.panel -->
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <!-- /.row -->
</div>
<script>
$(document).ready(function() {
    $('#dataTables-example2').DataTable({
        responsive: true
    });
});
</script>