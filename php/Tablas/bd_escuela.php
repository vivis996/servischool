<?php
 
include "conexion.php";
$user_id=null;
$sql1= "select * from bd_escuela";
$query = $con->query($sql1);

?>
<div class="panel panel-default">
    <br>
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <a data-toggle="modal" data-target="#myModal" class="btn btn-default">Agregar <i class="glyphicon glyphicon-plus-sign"></i></a>
        </div>
    </div>
    <br><br>
    <!-- Modal -->
    <div class="modal" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title">Agregar Alumno</h4>
                </div>
                <div class="modal-body">
                    <form role="form" method="post" action="./php/agregarlos.php">
                        <div class="form-group">
                            <label for="matricula">Matricula</label>
                            <input type="text" class="form-control" name="matricula" required>
                        </div>
                        <div class="form-group">
                            <label for="nombre">Nombre</label>
                            <input type="text" class="form-control" name="nombre" required>
                        </div>
                        <div class="form-group">
                            <label for="apellido">Apellidos</label>
                            <input type="text" class="form-control" name="apellido" required>
                        </div>
                        <div class="form-group">
                            <label for="turno">Turno</label>
                            <input type="text" class="form-control" name="turno" >
                        </div>
                        <!--ondita para el select-->
                        <div class="form-group">
                            <?php
                            $conexion = mysqli_connect("localhost","root","","servischool");
//error_reporting(E_ALL ^ E_NOTICE);
                            $consulta="SELECT * FROM carreras";
                            $resultado=mysqli_query($conexion,$consulta); //ejecutamos la consulta pero primero pasamos la conexion
                            echo"<div class='form-group'>";
                            echo"<label for='carreras_id_carrera'>Carreras:</label>";
                            echo"<select class='form-control' id='carreras_id_carrera' name='carreras_id_carrera' data-style='btn-primary'>";
                            while($registro=mysqli_fetch_array($resultado)){
                                echo "<option >".$registro['id_carrera']."</option><br>";
                            }
                            echo "</select>";
                            mysqli_close($conexion);      
                            echo"</div>";
                            ?>
                        </div>
                        <!--termina el select-->
                        <button type="submit" class="btn btn-default" name="btn-bsave">Agregar</button>
                    </form>
                </div>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="panel panel-default">
                <div class="panel-heading">
                    Registro de los alumnos Universidad Politecnica.
                </div>
                <!-- /.panel-headin -->
                <div class="panel-body">
                    <table width="" class="table table-striped table-bordered table-responsive table-hover" id="dataTables-example">
                        <thead>
                            <tr>
                            <th>Matricula</th>
                            <th>Nombre</th>
                            <th>Apellido</th>
                            <th>Turno</th>
                            <th>Carreras</th>
                            <th>Acciones</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php while ($r=$query->fetch_array()):?>
                            <tr>
                            <td><?php echo $r["matricula"]; ?></td>
                            <td><?php echo $r["nombre"]; ?></td>
                            <td><?php echo $r["apellido"]; ?></td>
                            <td><?php echo $r["turno"]; ?></td>
                            <td><?php echo $r["carreras_id_carrera"]; ?></td>
                                <td style="width:150px;">
                                    <a href="./editar.php?matricula=<?php echo $r["matricula"];?>" class="btn btn-sm btn-warning">Editar</a>
                                    <a href="#" id="del-<?php echo $r["matricula"];?>" class="btn btn-sm btn-danger">Eliminar</a>
                                    <script>
                                        $('"<?php echo "#del".$r["matricula"];?>"').click(function(e) {
                                            e.preventDefault();
                                            p = confirm("Estas seguro?");
                                            if(p){
                                                window.location="./php/Eliminar/eliminar.php?matricula="+<?php echo $r["matricula"];?>;

                                            }
                                        });
                                    </script>
                                </td>
                            </tr>
                            <?php endwhile;?>
                        </tbody>
                    </table>
                    <!-- /.table-responsive -->
                </div>
                <!-- /.panel-body -->
            </div>
            <!-- /.panel -->
        </div>
    </div>
</div>
<script>
$(document).ready(function() {
    $('#dataTables-example').DataTable({
        responsive: true
    });
});
</script>