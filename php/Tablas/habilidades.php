<?php
 
include "conexion.php";
$user_id=null;
$sql1= "select * from habilidades";
$query = $con->query($sql1);

?>
<div class="panel panel-default">
            <!-- /.row -->
<!-- Button trigger modal -->
<br>
    
        <div class="row">
            <div class="col-md-10 col-md-offset-1">
            <a data-toggle="modal" data-target="#myModal3" class="btn btn-default">Agregar <i class="glyphicon glyphicon-plus-sign"></i></a>
            </div>
        </div>
<br><br>
  <!-- Modal -->
  <div class="modal" id="myModal3" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
          <h4 class="modal-title">Agregar Habilidad</h4>
        </div>
        <div class="modal-body">
<form role="form" method="post" action="php/agregarlos.php">
  <div class="form-group">
    <label for="matricula">Id_Habilidad</label>
    <input type="text" class="form-control" name="matricula" required>
  </div>
  <div class="form-group">
    <label for="nombre">Nombre</label>
    <input type="text" class="form-control" name="nombre" required>
  </div>
  </div>

      </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
  </div><!-- /.modal -->

        <div class="row">
            <div class="col-md-10 col-md-offset-1">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            Todas las tablas
                        </div>
                        <!-- /.panel-headin -->
                        <div class="panel-body">
                            <table width="100%" class="table table-striped table-bordered table-hover" id="dataTables-example3">
                            <thead>
                            <tr>
                              <th>Id_Habilidad</th>
                              <th>Nombre</th>
                              <th>Acciones</th>
                              </tr>
                            </thead>
                            <?php while ($r=$query->fetch_array()): ?>
                            <tr>
                              <td><?php echo $r["idhabilidades"]; ?> </td>
                              <td><?php echo utf8_encode ($r["categorias"]);?></td>
                              <td style="width:150px;">
                                <a href="./editar.php?idhabilidades=<?php echo $r["idhabilidades"];?>" class="btn btn-sm btn-warning">Editar</a>
                                <a href="#" id="del-<?php echo $r["idhabilidades"];?>" class="btn btn-sm btn-danger">Eliminar</a>
                                <script>
                                $('"<?php echo "#del".$r["idhabilidades"];?>"').click(function(e){
                                  e.preventDefault();
                                  p = confirm("Estas seguro?");
                                  if(p){
                                    window.location="./php/eliminar.php?idhabilidades="+<?php echo $r["idhabilidades"];?>;

                                  }

                                });
                                </script>
                                 </td>
                                        </tr>
                                        <?php endwhile;?>
                                </tbody>
                            </table>
                            <!-- /.table-responsive -->
                        </div>
                        <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
        </div>
        <!-- /#page-wrapper -->

    <!-- /#wrapper -->

    <!-- jQuery -->

    <!-- Page-Level Demo Scripts - Tables - Use for reference -->
    <script>
    $(document).ready(function() {
        $('#dataTables-example3').DataTable({
            responsive: true
        });
    });
    </script>
