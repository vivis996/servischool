<?php

include "conexion.php";

$user_id=null;
$sql1= "select * from bd_escuela where matricula like '%$_GET[s]%' or nombre like '%$_GET[s]%' or apellido like '%$_GET[s]%' or turno like '%$_GET[s]%' or carreras_id_carrera like '%$_GET[s]%' ";
$query = $con->query($sql1);
?>

<?php if($query->num_rows>0):?>
<table class="table table-striped table-bordered table-responsive table-hover">
<thead>
	<th>Matricula</th>
	<th>Nombre</th>
	<th>Apellido</th>
	<th>Turno</th>
	<th>Carrera</th>
	<th>Acciones</th>
</thead>
<?php while ($r=$query->fetch_array()):?>
<tr>
	<td><?php echo $r["matricula"]; ?></td>
	<td><?php echo $r["nombre"]; ?></td>
	<td><?php echo $r["apellido"]; ?></td>
	<td><?php echo $r["turno"]; ?></td>
	<td><?php echo $r["carreras_id_carrera"]; ?></td>
	<td style="width:150px;">
		<a href="./editar.php?matricula=<?php echo $r["matricula"];?>" class="btn btn-sm btn-warning">Editar</a>
		<a href="#" id="del-<?php echo $r["matricula"];?>" class="btn btn-sm btn-danger">Eliminar</a>
		<script>
		$("#del-"+<?php echo $r["matricula"];?>).click(function(e){
			e.preventDefault();
			p = confirm("Estas seguro?");
			if(p){
				window.location="./php/eliminar.php?matricula="+<?php echo $r["matricula"];?>;

			}

		});
		</script>
	</td>
</tr>
<?php endwhile;?>
</table>
<?php else:?>
	<p class="alert alert-warning">No hay resultados</p>
<?php endif;?>
