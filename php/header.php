<html>
<meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minium-scale=1.0">
<html lang="en">
    <head>
        <meta charset="utf-8">
        <title>.: CRUD :.</title>
        <link rel="stylesheet" type="text/css" href="./bootstrap/css/bootstrap.min.css">
        <link href="./vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
        <link href="./vendor/metisMenu/metisMenu.min.css" rel="stylesheet">
        <link href="./vendor/datatables-plugins/dataTables.bootstrap.css" rel="stylesheet">
        <link href="./vendor/datatables-responsive/dataTables.responsive.css" rel="stylesheet">
        <link href="./dist/css/sb-admin-2.css" rel="stylesheet">
        <link href="./vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css"> 
        <script src="./js/jquery.min.js"></script>
    </head>
    <body >
        <?php include "php/navbar.php"; ?>