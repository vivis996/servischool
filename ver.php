<?php include "php/header.php"; ?>
    <!--<div align="center"><center>-->
    <div class="row">
        <div class="col-md-6 col-md-offset-1">
            <h2>Tablas</h2>
        </div>
    </div>
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
        <ul class="nav nav-tabs" style="text-align:center;">
            <li class="active"><a data-toggle="tab" href="#home">Alumnos</a></li>
            <li><a data-toggle="tab" href="#carreras">Carreras</a></li>
            <li><a data-toggle="tab" href="#habilidades">Habilidades</a></li>
            <li><a data-toggle="tab" href="#mensajesrap">Mensajes rapidos</a></li>
        </ul>
        <div class="tab-content"><!--tabla de alumnos de la universidad-->
            <div id="home" class="tab-pane fade in active" >
                <?php include "php/Tablas/bd_escuela.php"; ?>
            </div>
            <div id="carreras" class="tab-pane fade"><!--tabla de Carreras de la universidad-->
                <?php include "php/Tablas/carreraas.php"; ?>
            </div>
            <div id="habilidades" class="tab-pane fade"><!--tabla de Habilidades-->
                <?php include "php/Tablas/habilidades.php"; ?>
            </div>
            <div id="mensajesrap" class="tab-pane fade"><!--tabla de Mensajes Rapidos-->
                <?php include "php/Tablas/mensajesrap.php"; ?>
            </div>
            </div>
        </div>
    </div>
<?php include "php/footer.php"?>